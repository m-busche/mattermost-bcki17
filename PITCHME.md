# Mattermost

## Die Open Source Slack Alternative

---

### Mattermost, Slack, HipChat & Co.?

 - Gruppenchat
 - Kommunikation, Zusammenarbeit
 - Für Unternehmen konzipiert
 - Auch für private Gruppen einsetzbar (Familie, Freundeskreis)
 - IRC für nicht-Nerds

---

 ### Login-Screen Desktop Client

 ![Mattermost Login](http://i.imgur.com/CHNsXE1.png)

Mattermost Client Login Screen

---

 ### Desktop-Client

 ![Deskop Client](http://i.imgur.com/rI3EJcu.png)

---

### Links

| ![Sign Up](http://i.imgur.com/vmVXehI.png)
(http://2d71c1b0.ngrok.io) | ![Desktop Client](http://i.imgur.com/DEAbKAM.png) (https://github.com/mattermost/desktop/releases) | 
| ![Matterhorn, Konsolen Client](http://i.imgur.com/MELJzBk.png)
(https://github.com/matterhorn-chat/matterhorn/releases) | ![Mattermost Homepage](http://i.imgur.com/MELJzBk.png) |


---

